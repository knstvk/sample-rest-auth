## Implementation

Service: `com.company.restauth.service.FooServiceBean`, published in REST in `com/company/restauth/rest-services.xml`

Custom controller: `com.company.restauth.controllers.BarController`, published by `com/company/restauth/rest-dispatcher-spring.xml` which is in turn registered in `com/company/restauth/web-app.properties`.  

## Usage

Authenticate (obtain an OAuth token):
```
curl -X POST \
  http://localhost:8080/app/rest/v2/oauth/token \
  -H 'Authorization: Basic Y2xpZW50OnNlY3JldA==' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'grant_type=password&username=admin&password=admin'
```
Response: `{"access_token":"3e7256e4-7dc5-4d50-b887-9b5fdc8b99a9","token_type":"bearer","refresh_token":"5736605b-8350-4cd3-b0d5-fa5a80662851","expires_in":43199,"scope":"rest-api"}`

Request service (you have to provide `access_token` in `Authorization` header):
```
curl -X GET \
  'http://localhost:8080/app/rest/v2/services/restauth_FooService/echo?value=abc' \
  -H 'Authorization: Bearer 3e7256e4-7dc5-4d50-b887-9b5fdc8b99a9'
```

Request custom controller (no authentication is needed):
```
curl -X GET 'http://localhost:8080/app/rest/bar/echo?value=cba'
```