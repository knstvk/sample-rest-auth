package com.company.restauth.service;

import org.springframework.stereotype.Service;

@Service(FooService.NAME)
public class FooServiceBean implements FooService {

    @Override
    public String echo(String value) {
        return value;
    }
}