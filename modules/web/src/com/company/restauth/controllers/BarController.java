package com.company.restauth.controllers;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.sys.AppContext;
import com.haulmont.cuba.core.sys.SecurityContext;
import com.haulmont.cuba.security.app.TrustedClientService;
import com.haulmont.cuba.security.entity.User;
import com.haulmont.cuba.security.global.LoginException;
import com.haulmont.cuba.security.global.UserSession;
import com.haulmont.cuba.web.auth.WebAuthConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;
import java.util.function.Supplier;

@RestController
@RequestMapping("/bar")
public class BarController {

    @Inject
    private TrustedClientService trustedClientService;

    @Inject
    private WebAuthConfig webAuthConfig;

    @Inject
    private DataManager dataManager;

    private static final Logger log = LoggerFactory.getLogger(BarController.class);

    @GetMapping("/echo")
    public ResponseEntity<String> echo(String value) {
        return runAsAnonymous(() -> {
            List<User> users = dataManager.load(User.class).list();
            return value + " - users: " + users;
        });
    }

    private <T> ResponseEntity<T> runAsAnonymous(Supplier<T> function) {
        try {
            UserSession anonymousSession = trustedClientService.getAnonymousSession(webAuthConfig.getTrustedClientPassword());
            AppContext.setSecurityContext(new SecurityContext(anonymousSession));
            try {
                return new ResponseEntity<>(function.get(), HttpStatus.OK);
            } finally {
                AppContext.setSecurityContext(null);
            }
        } catch (LoginException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (Exception e) {
            log.error("Error executing request", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
